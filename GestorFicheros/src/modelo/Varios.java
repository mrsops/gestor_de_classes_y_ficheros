package modelo;

public class Varios extends Articulo{
    private double kilos;

    public Varios(int id, String nombre, double precio, double kilos) {
        super(id, nombre, precio);
        this.kilos = kilos;
    }

    public double getKilos() {
        return kilos;
    }

    public void setKilos(double kilos) {
        this.kilos = kilos;
    }
    
    
}
