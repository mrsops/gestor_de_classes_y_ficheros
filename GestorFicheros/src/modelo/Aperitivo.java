package modelo;

import modelo.Articulo;
import modelo.Embalaje;

public class Aperitivo extends Articulo {
    private Embalaje tipo;

    public Aperitivo(int id, String nombre, double precio, Embalaje tipo) {
        super(id, nombre, precio);
        this.tipo = tipo;
    }

    public Embalaje getTipo() {
        return tipo;
    }

    public void setTipo(Embalaje tipo) {
        this.tipo = tipo;
    }
    
    
}
