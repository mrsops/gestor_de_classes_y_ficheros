package modelo;

import java.io.Serializable;

public enum Embalaje implements Serializable{
    BOLSA,
    BREAK,
    LATA;
}
