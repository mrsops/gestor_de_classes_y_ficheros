package modelo;

import java.io.Serializable;
import java.util.ArrayList;

public class Cliente implements Serializable{
    private int id;
    private String nombre;
    private ArrayList<Articulo> articulos;

    public Cliente(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
        this.articulos = new ArrayList<Articulo>();
    }

    public void anyadirArticulo(Articulo articulo){
        this.articulos.add(articulo);
    }
    
    public Articulo buscarArticulo(String nombre){
        for (Articulo articulo : articulos) {
            if(articulo.getNombre().equals(nombre)){ return articulo;}
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Articulo> getArticulos() {
        return articulos;
    }

    public void setArticulos(ArrayList<Articulo> articulos) {
        this.articulos = articulos;
    }
    
    public int numeroArticulos(){
        return this.articulos.size();
    }
    
    public void borrarArticulo(Articulo a){
        this.articulos.remove(a);
    }
}
