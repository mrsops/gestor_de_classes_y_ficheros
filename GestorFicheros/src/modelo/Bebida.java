package modelo;

public class Bebida extends Articulo {
    private double litros;

    public Bebida(int id, String nombre, double precio, double litros) {
        super(id, nombre, precio);
        this.litros = litros;
    }

    public double getLitros() {
        return litros;
    }

    public void setLitros(double litros) {
        this.litros = litros;
    }
    
    
}
