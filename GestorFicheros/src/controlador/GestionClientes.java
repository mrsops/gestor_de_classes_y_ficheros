/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.Serializable;
import java.util.ArrayList;
import modelo.Cliente;

/**
 *
 * @author mrsops
 */
public class GestionClientes implements Serializable{
     static ArrayList <Cliente> clientes=new ArrayList <Cliente>();
     static Cliente clienteActivo;
    
    public static void añadirCliente(Cliente c){
        clientes.add(c);
    }
    
    public static Cliente buscaCliente(String nom){
        Cliente cli=null;
        int i=0;
        boolean encontrado=false;
        while(i<clientes.size() && !encontrado){
            if(clientes.get(i).getNombre().equals(nom)){
                cli=clientes.get(i);
                encontrado=true;
            }
            i++;
        }
        return cli;   
    }
    
    public static ArrayList <Cliente> obtenerClientes(){
        return clientes;
    }
    
    public static void setClientes(ArrayList<Cliente> listaClientes){
        clientes = listaClientes;
    }
    
    public static void setClienteActivo(Cliente c){
        clienteActivo = c;
    }
    
    public static Cliente getClienteActivo(){
        return clienteActivo;
    }
    
    public static void borrarCliente(Cliente cli){
        clientes.remove(cli);
    }
    
}
