package controlador;

import modelo.Aperitivo;
import modelo.Articulo;
import modelo.Bebida;
import modelo.Cliente;
import modelo.Embalaje;
import modelo.Varios;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GestionFicheros {

    private File archivo;
    private File raiz;
    private File binario;
    public GestionFicheros(File archivoCsv, File carpetaRaiz, File binario) {
        this.archivo = archivoCsv;
        this.raiz = carpetaRaiz;
        this.binario = binario;
    }

    public GestionFicheros(String archivoCsv, String carpetaRaiz, String binario) {
        this.archivo = new File(archivoCsv);
        this.raiz = new File(carpetaRaiz);
        this.binario = new File(binario);
    }

    /**
     * Pasa de CSV a LISTA.
     *
     * @return retorna un ARRAYLIST.
     * @throws FileNotFoundException
     */
    public ArrayList<Cliente> deCsvALista() throws FileNotFoundException {

        Cliente cliente = null;
        Articulo articulo = null;
        ArrayList<Cliente> lista = new ArrayList<Cliente>();
        Scanner readArchivo = null;
        readArchivo = new Scanner(this.archivo);

        while (readArchivo.hasNext()) {
            String linea = readArchivo.nextLine();
            String[] campos = linea.split(";");
            if (campos[0] != null && campos[1] != null) {
                int id = Integer.parseInt(campos[0]);
                cliente = new Cliente(id, campos[1]);
            }
            int multiplicador = 0;
            int inicio = 2;
            int lecturas = (campos.length - 2) / 5;
            for (int i = 0; i < lecturas; i++) {
                int posicion = (inicio + (multiplicador * 5));
                int codProd = Integer.parseInt(campos[posicion + 0]);
                String nomProd = campos[posicion + 1];
                double precioProd = Double.parseDouble(campos[posicion + 2]);
                String tipoProd = campos[posicion + 3];
                if (tipoProd.equals("Aperitivo")) {
                    Embalaje eb = null;
                    if (campos[posicion + 4].equals("Bolsa")) {
                        eb = Embalaje.BOLSA;
                    } else if (campos[posicion + 4].equals("Break")) {
                        eb = Embalaje.BREAK;
                    } else if (campos[posicion + 4].equals("Lata")) {
                        eb = Embalaje.LATA;
                    }
                    articulo = new Aperitivo(codProd, nomProd, precioProd, eb);
                } else if (tipoProd.equals("Bebida")) {
                    double litros = Double.parseDouble(campos[posicion + 4]);
                    articulo = new Bebida(codProd, nomProd, precioProd, litros);
                } else if (tipoProd.equals("Varios")) {
                    double kilos = Double.parseDouble(campos[posicion + 4]);
                    articulo = new Varios(codProd, nomProd, precioProd, kilos);
                }
                cliente.anyadirArticulo(articulo);
                multiplicador++;
            }

            lista.add(cliente);

        }
        readArchivo.close();
        return lista;
    }

    /**
     * Pasa de LISTA a CARPETA.
     *
     * @param clientes
     * @throws FileNotFoundException
     */
    public void deListaACarpeta(ArrayList<Cliente> clientes) throws FileNotFoundException {
        File carpeta = null;
        File base = this.raiz;
        borrarDirectorio(base);
        base.mkdir();
        PrintWriter datos = null;
        PrintWriter cliente = null;
        for (Cliente c : clientes) {

            base.mkdir();
            carpeta = new File(base.getPath() + "/" + c.getNombre());
            carpeta.mkdir();
            cliente = new PrintWriter(new File(carpeta.getPath() + "/" + "idCliente.dat"));
            cliente.print(c.getId() + ";");

            for (Articulo a : c.getArticulos()) {

                try {
                    datos = new PrintWriter(new File(carpeta.getPath() + "/" + "000" + a.getId() + ".dat"));

                    if (a instanceof Aperitivo) { //SI ES INSTANCIA DE APERITIVO, TENEMOS QUE FILTRAR POR LOS ENUM.
                        Aperitivo ap = (Aperitivo) a;
                        if (ap.getTipo() == Embalaje.BOLSA) {
                            datos.print(a.getId() + ";" + a.getNombre() + ";" + a.getPrecio() + ";" + "Aperitivo;Bolsa;");
                        } else if (ap.getTipo() == Embalaje.BREAK) {
                            datos.print(a.getId() + ";" + a.getNombre() + ";" + a.getPrecio() + ";" + "Aperitivo;Break;");
                        } else if (ap.getTipo() == Embalaje.LATA) {
                            datos.print(a.getId() + ";" + a.getNombre() + ";" + a.getPrecio() + ";" + "Aperitivo;Lata;");
                        }

                    } else if (a instanceof Bebida) {
                        Bebida b = (Bebida) a;
                        String litros = Double.toString(b.getLitros());
                        datos.print(a.getId() + ";" + a.getNombre() + ";" + a.getPrecio() + ";" + "Bebida;" + litros + ";");
                    } else if (a instanceof Varios) {
                        Varios v = (Varios) a;
                        String kilos = Double.toString(v.getKilos());
                        datos.print(a.getId() + ";" + a.getNombre() + ";" + a.getPrecio() + ";" + "Bebida;" + kilos + ";");
                    }
                } catch (FileNotFoundException e) {
                    throw e;
                } finally {
                    datos.close();     
                }
            }
            cliente.close();
        }

    }

    public void deCarpetaACsv() {
        PrintWriter csv = null;
        String linea = "";
        String nombreCliente = "";
        String id = "";
        Cliente client = null;
        Articulo prod = null;
        try {
            File rutaCarpeta = this.raiz;
            csv = new PrintWriter(this.archivo);

            for (File f : rutaCarpeta.listFiles()) { // Por cada carpeta, crearemos un cliente, y por cada cliente
                nombreCliente = f.getName();
                File[] archivos = f.listFiles();
                String[] campos = null;
                String clienteID = "";
                linea = "";
                String leerLinea;
                if (f.isDirectory()) {

                    for (File a : archivos) {
                        if (!a.getName().equals("idCliente.dat")) {
                            Scanner leer = new Scanner(a);
                            leerLinea = leer.nextLine();
                            campos = leerLinea.split(";");
                            String idProd = campos[0];
                            String nomProd = campos[1];
                            String precio = campos[2];
                            String tipo = campos[3];
                            String cantidad = campos[4];
                            linea += idProd + ";" + nomProd + ";" + precio + ";" + tipo + ";" + cantidad+";";

                        } else if (a.getName().equals("idCliente.dat")) {
                            Scanner leerIDCliente = new Scanner(a);
                            
                            clienteID = leerIDCliente.nextLine().split(";")[0];
                            leerIDCliente.close();
                            clienteID += ";"+f.getName() + ";";
                        }

                    }
                    

                }
                csv.print(clienteID + linea + "\n");
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GestionFicheros.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            csv.close();
        }

    }
    public void copiaSeguridad() throws FileNotFoundException, IOException {
        
        try (ObjectOutputStream salida = new ObjectOutputStream(new FileOutputStream(this.binario))) {
            salida.writeObject(GestionClientes.obtenerClientes());
        }
    
    }//c.serializar
    
    public void leerCopiaSeguridad() throws FileNotFoundException, IOException, ClassNotFoundException{
        try (ObjectInputStream entrada = new ObjectInputStream(new FileInputStream(this.binario))) {
            GestionClientes.setClientes((ArrayList<Cliente>)entrada.readObject());
        }
    }//c.serializar
    
    private void borrarDirectorio(File directorio){
        
        if (directorio.isDirectory()){
            File[] archivos = directorio.listFiles();
            for(File f:archivos){
                if(f.isDirectory()){
                    borrarDirectorio(f);
                }else{
                    f.delete();
                }
            }
            directorio.delete();
        }
    }
    
}
